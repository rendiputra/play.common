﻿using System;

namespace Play.Cummon
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
